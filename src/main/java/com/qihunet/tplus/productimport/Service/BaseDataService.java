package com.qihunet.tplus.productimport.Service;

import com.qihunet.tplus.productimport.entity.CostAllocationOrder;
import com.qihunet.tplus.productimport.entity.InventoryEntityDTO;
import com.qihunet.tplus.productimport.entity.UnitDTO;
import com.qihunet.tplus.productimport.utils.ReflectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class BaseDataService {
    @Autowired
    private JdbcTemplate jdbcTemplate;


    /**
     * 获取所有的商品信息
     * @return
     */
    public List<InventoryEntityDTO> findAllGoodsInfo(){
        String sql="SELECT code, name, shorthand, id,idunit,idwarehouse" +
                " FROM AA_Inventory";
        return (List<InventoryEntityDTO>) jdbcTemplate.query(sql, new RowMapper<InventoryEntityDTO>(){
            @Override
            public InventoryEntityDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
                ResultSetMetaData rSetMetaData=rs.getMetaData();
                Map<String, Object> maps=new HashMap<String, Object>();
                InventoryEntityDTO inventoryEntityDTO = new InventoryEntityDTO();
                for(int i=0;i<rSetMetaData.getColumnCount();i++){
                    String columnLabel=rSetMetaData.getColumnLabel(i+1);
                    Object columnValue=rs.getObject(i+1);
                    maps.put(columnLabel,columnValue);
                }
                if(maps.size()>0){
                    for(Map.Entry<String,Object> entry:maps.entrySet()){
                        String fieldName=entry.getKey();
                        Object fieldValue=entry.getValue();
                        ReflectionUtils.setFieldValue(inventoryEntityDTO, fieldName, fieldValue);
                    }
                }
                return inventoryEntityDTO;
            }
        });
    }

    /**
     * 获取所有的单位信息
     * @return
     */
    public List<UnitDTO> findAllUnitInfo(){
        String sql="SELECT code, name, id FROM AA_Unit";
        return (List<UnitDTO>) jdbcTemplate.query(sql, new RowMapper<UnitDTO>(){
            @Override
            public UnitDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
                ResultSetMetaData rSetMetaData=rs.getMetaData();
                Map<String, Object> maps=new HashMap<String, Object>();
                UnitDTO unitDTO = new UnitDTO();
                for(int i=0;i<rSetMetaData.getColumnCount();i++){
                    String columnLabel=rSetMetaData.getColumnLabel(i+1);
                    Object columnValue=rs.getObject(i+1);
                    maps.put(columnLabel,columnValue);
                }
                if(maps.size()>0){
                    for(Map.Entry<String,Object> entry:maps.entrySet()){
                        String fieldName=entry.getKey();
                        Object fieldValue=entry.getValue();
                        ReflectionUtils.setFieldValue(unitDTO, fieldName, fieldValue);
                    }
                }
                return unitDTO;
            }
        });
    }
}
