package com.qihunet.tplus.productimport.utils;

import java.io.Serializable;
import java.util.List;

public class ResultInfo<T> implements Serializable {
    private int resultCode;
    private String resultData;
    private String errorMessage;
    private PageModel pageModel;
    private T data;
    private List<T> dataList;
    private Object relationId;

    public ResultInfo() {
    }

    public String getResultData() {
        return this.resultData;
    }

    public void setResultData(String resultData) {
        this.resultData = resultData;
    }

    public String getErrorMessage() {
        return this.errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public int getResultCode() {
        return this.resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    public List<T> getDataList() {
        return this.dataList;
    }

    public void setDataList(List<T> dataList) {
        this.dataList = dataList;
    }

    public PageModel getPageModel() {
        return this.pageModel;
    }

    public void setPageModel(PageModel pageModel) {
        this.pageModel = pageModel;
    }

    public T getData() {
        return this.data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public Object getRelationId() {
        return this.relationId;
    }

    public void setRelationId(Object relationId) {
        this.relationId = relationId;
    }

    public String toString() {
        return "ResultInfo [resultCode=" + this.resultCode + ", resultData=" + this.resultData + ", errorMessage=" + this.errorMessage + ", pageModel=" + this.pageModel + ", data=" + this.data + ", dataList=" + this.dataList + ", relationId=" + this.relationId + "]";
    }
}
