package com.qihunet.tplus.productimport.utils;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;

/**
 * 反射处理类
 * @author gary
 *
 */
public class ReflectionUtils {
    /**
     * 直接设置对象属性值
     * @param obj 类
     * @param filedName 字段名称
     * @param value 值
     */
    public static void setFieldValue(Object obj,String filedName,Object value){
        Field field=getDeclaredField(obj, filedName);
        if(field==null){
            return;
        }
        makeAccessible(field);
        try {
            field.set(obj, value);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * 使 filed 变为可访问
     * @param field
     */
    public static void makeAccessible(Field field){
        if(!Modifier.isPublic(field.getModifiers())){
            field.setAccessible(true);
        }
    }

    /**
     * 循环向上转型, 获取对象的 DeclaredField
     * @param obj 类
     * @param filedName 字段名称
     * @return
     */
    private static Field getDeclaredField(Object obj, String filedName) {
        for(Class<?> supperClass=obj.getClass();supperClass!=Object.class;supperClass=supperClass.getSuperclass()){
            try {
                return supperClass.getDeclaredField(filedName);
            } catch (SecurityException e) {
                e.printStackTrace();
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    /**
     * 获取泛型实例
     * @param <T>
     * @param obj
     * @return
     */
    @SuppressWarnings("unchecked")
    public static <T> Class<T> getClassT(Object obj){
        Class<T> clazz=(Class<T>)((ParameterizedType)obj.getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        return clazz;
    }
}
