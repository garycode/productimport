package com.qihunet.tplus.productimport.utils;


import java.io.Serializable;
import java.util.List;

public class PageModel<E> implements Serializable {
    private List<E> list;
    private int totalRecords;
    private int pageSize;
    private int pageNum;

    public PageModel() {
    }

    public int getTotalPages() {
        return (this.totalRecords + this.pageSize - 1) / this.pageSize;
    }

    public int getToppageNum() {
        return 1;
    }

    public int getPreviouspageNum() {
        return this.getPageNum() <= 1 ? 1 : this.getPageNum() - 1;
    }

    public int getNextpageNum() {
        return this.getPageNum() >= this.getBottompageNum() ? this.getBottompageNum() : this.getPageNum() + 1;
    }

    public int getBottompageNum() {
        return this.getTotalPages();
    }

    public List<E> getList() {
        return this.list;
    }

    public void setList(List<E> list) {
        this.list = list;
    }

    public int getTotalRecords() {
        return this.totalRecords;
    }

    public void setTotalRecords(int totalRecords) {
        this.totalRecords = totalRecords;
    }

    public int getPageSize() {
        return this.pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String toString() {
        return "PageModel [list=" + this.list + ", totalRecords=" + this.totalRecords + ", pageSize=" + this.pageSize + ", pageNum=" + this.getPageNum() + "]";
    }

    public int getPageNum() {
        return this.pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }
}
