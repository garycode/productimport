package com.qihunet.tplus.productimport.entity;

/**
 * @author
 * @create 2017-11-20 下午10:31
 **/
public class UserDTO {
    private Integer id;
    private Long code;
    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Long getCode() {
        return code;
    }

    public void setCode(long code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
