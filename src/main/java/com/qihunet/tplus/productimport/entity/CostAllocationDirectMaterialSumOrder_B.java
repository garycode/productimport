package com.qihunet.tplus.productimport.entity;

import java.math.BigDecimal;
import java.math.BigDecimal;
import java.util.Date;

/**
 *  产品成本分配材料分配明细子表
 */
public class CostAllocationDirectMaterialSumOrder_B {
    private String code;
    private BigDecimal quantity;
    private BigDecimal nassignedquantity;
    private BigDecimal nassignedcost;
    private BigDecimal assignedquantity;
    private BigDecimal assignedcost;
    private BigDecimal balancequantity;
    private BigDecimal balancecost;
    private BigDecimal origassignedquantity;
    private BigDecimal totalassignedquantity;
    private BigDecimal sequencenumber;
    private BigDecimal ckcost;
    private Integer id;
    private Integer idinventory;
    private Integer idbaseunit;
    private Integer idCostAllocationDirectMaterialSumOrderDTO;
    private Integer ManufactureOrderMaterialDetailId;
    private Date createdtime;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getNassignedquantity() {
        return nassignedquantity;
    }

    public void setNassignedquantity(BigDecimal nassignedquantity) {
        this.nassignedquantity = nassignedquantity;
    }

    public BigDecimal getNassignedcost() {
        return nassignedcost;
    }

    public void setNassignedcost(BigDecimal nassignedcost) {
        this.nassignedcost = nassignedcost;
    }

    public BigDecimal getAssignedquantity() {
        return assignedquantity;
    }

    public void setAssignedquantity(BigDecimal assignedquantity) {
        this.assignedquantity = assignedquantity;
    }

    public BigDecimal getAssignedcost() {
        return assignedcost;
    }

    public void setAssignedcost(BigDecimal assignedcost) {
        this.assignedcost = assignedcost;
    }

    public BigDecimal getBalancequantity() {
        return balancequantity;
    }

    public void setBalancequantity(BigDecimal balancequantity) {
        this.balancequantity = balancequantity;
    }

    public BigDecimal getBalancecost() {
        return balancecost;
    }

    public void setBalancecost(BigDecimal balancecost) {
        this.balancecost = balancecost;
    }

    public BigDecimal getOrigassignedquantity() {
        return origassignedquantity;
    }

    public void setOrigassignedquantity(BigDecimal origassignedquantity) {
        this.origassignedquantity = origassignedquantity;
    }

    public BigDecimal getTotalassignedquantity() {
        return totalassignedquantity;
    }

    public void setTotalassignedquantity(BigDecimal totalassignedquantity) {
        this.totalassignedquantity = totalassignedquantity;
    }

    public BigDecimal getSequencenumber() {
        return sequencenumber;
    }

    public void setSequencenumber(BigDecimal sequencenumber) {
        this.sequencenumber = sequencenumber;
    }

    public BigDecimal getCkcost() {
        return ckcost;
    }

    public void setCkcost(BigDecimal ckcost) {
        this.ckcost = ckcost;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdinventory() {
        return idinventory;
    }

    public void setIdinventory(Integer idinventory) {
        this.idinventory = idinventory;
    }

    public Integer getIdbaseunit() {
        return idbaseunit;
    }

    public void setIdbaseunit(Integer idbaseunit) {
        this.idbaseunit = idbaseunit;
    }

    public Integer getIdCostAllocationDirectMaterialSumOrderDTO() {
        return idCostAllocationDirectMaterialSumOrderDTO;
    }

    public void setIdCostAllocationDirectMaterialSumOrderDTO(Integer idCostAllocationDirectMaterialSumOrderDTO) {
        this.idCostAllocationDirectMaterialSumOrderDTO = idCostAllocationDirectMaterialSumOrderDTO;
    }

    public Integer getManufactureOrderMaterialDetailId() {
        return ManufactureOrderMaterialDetailId;
    }

    public void setManufactureOrderMaterialDetailId(Integer manufactureOrderMaterialDetailId) {
        ManufactureOrderMaterialDetailId = manufactureOrderMaterialDetailId;
    }

    public Date getCreatedtime() {
        return createdtime;
    }

    public void setCreatedtime(Date createdtime) {
        this.createdtime = createdtime;
    }
}
