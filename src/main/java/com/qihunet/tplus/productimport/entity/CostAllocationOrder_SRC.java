package com.qihunet.tplus.productimport.entity;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 产品成本分配选单来源表 
 */
public class CostAllocationOrder_SRC {
    private String sourceVoucherCode;
    private BigDecimal refcost;
    private BigDecimal totalrefcost;
    private BigDecimal quantity;
    private String jgdcode;
    private String rkdcode;
    private Integer id;
    private Integer  idinventory;
    private Integer idproject;
    private Integer idbaseunit;
    private Integer idWarehouse;
    private Integer idCostAllocationOrderDTO;
    private Integer voucherId;
    private Integer idSum;
    private Integer  jgdid;
    private Integer jgdDetailId;
    private Integer rkdid;
    private Integer sourceVoucherId;
    private Integer rkdDetailId;
    private Integer sourceVoucherDetailId;
    private Date createdtime;
    private Integer  SourceVoucherTypeId;

    public String getSourceVoucherCode() {
        return sourceVoucherCode;
    }

    public void setSourceVoucherCode(String sourceVoucherCode) {
        this.sourceVoucherCode = sourceVoucherCode;
    }

    public BigDecimal getRefcost() {
        return refcost;
    }

    public void setRefcost(BigDecimal refcost) {
        this.refcost = refcost;
    }

    public BigDecimal getTotalrefcost() {
        return totalrefcost;
    }

    public void setTotalrefcost(BigDecimal totalrefcost) {
        this.totalrefcost = totalrefcost;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public String getJgdcode() {
        return jgdcode;
    }

    public void setJgdcode(String jgdcode) {
        this.jgdcode = jgdcode;
    }

    public String getRkdcode() {
        return rkdcode;
    }

    public void setRkdcode(String rkdcode) {
        this.rkdcode = rkdcode;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdinventory() {
        return idinventory;
    }

    public void setIdinventory(Integer idinventory) {
        this.idinventory = idinventory;
    }

    public Integer getIdproject() {
        return idproject;
    }

    public void setIdproject(Integer idproject) {
        this.idproject = idproject;
    }

    public Integer getIdbaseunit() {
        return idbaseunit;
    }

    public void setIdbaseunit(Integer idbaseunit) {
        this.idbaseunit = idbaseunit;
    }

    public Integer getIdWarehouse() {
        return idWarehouse;
    }

    public void setIdWarehouse(Integer idWarehouse) {
        this.idWarehouse = idWarehouse;
    }

    public Integer getIdCostAllocationOrderDTO() {
        return idCostAllocationOrderDTO;
    }

    public void setIdCostAllocationOrderDTO(Integer idCostAllocationOrderDTO) {
        this.idCostAllocationOrderDTO = idCostAllocationOrderDTO;
    }

    public Integer getVoucherId() {
        return voucherId;
    }

    public void setVoucherId(Integer voucherId) {
        this.voucherId = voucherId;
    }

    public Integer getIdSum() {
        return idSum;
    }

    public void setIdSum(Integer idSum) {
        this.idSum = idSum;
    }

    public Integer getJgdid() {
        return jgdid;
    }

    public void setJgdid(Integer jgdid) {
        this.jgdid = jgdid;
    }

    public Integer getJgdDetailId() {
        return jgdDetailId;
    }

    public void setJgdDetailId(Integer jgdDetailId) {
        this.jgdDetailId = jgdDetailId;
    }

    public Integer getRkdid() {
        return rkdid;
    }

    public void setRkdid(Integer rkdid) {
        this.rkdid = rkdid;
    }

    public Integer getSourceVoucherId() {
        return sourceVoucherId;
    }

    public void setSourceVoucherId(Integer sourceVoucherId) {
        this.sourceVoucherId = sourceVoucherId;
    }

    public Integer getRkdDetailId() {
        return rkdDetailId;
    }

    public void setRkdDetailId(Integer rkdDetailId) {
        this.rkdDetailId = rkdDetailId;
    }

    public Integer getSourceVoucherDetailId() {
        return sourceVoucherDetailId;
    }

    public void setSourceVoucherDetailId(Integer sourceVoucherDetailId) {
        this.sourceVoucherDetailId = sourceVoucherDetailId;
    }

    public Date getCreatedtime() {
        return createdtime;
    }

    public void setCreatedtime(Date createdtime) {
        this.createdtime = createdtime;
    }

    public Integer getSourceVoucherTypeId() {
        return SourceVoucherTypeId;
    }

    public void setSourceVoucherTypeId(Integer sourceVoucherTypeId) {
        SourceVoucherTypeId = sourceVoucherTypeId;
    }
}
