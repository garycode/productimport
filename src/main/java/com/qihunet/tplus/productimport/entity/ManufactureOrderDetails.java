package com.qihunet.tplus.productimport.entity;

/**
 * 生产加工单产成品明细DTO
 */
public class ManufactureOrderDetails {
    private Integer id;
    private Integer idManufactureOrderDTO;
    private Integer idInventory;

    public Integer getIdInventory() {
        return idInventory;
    }

    public void setIdInventory(Integer idInventory) {
        this.idInventory = idInventory;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdManufactureOrderDTO() {
        return idManufactureOrderDTO;
    }

    public void setIdManufactureOrderDTO(Integer idManufactureOrderDTO) {
        this.idManufactureOrderDTO = idManufactureOrderDTO;
    }
}
