package com.qihunet.tplus.productimport.entity;

import java.math.BigDecimal;
import java.util.Date;

/**
 *  产品成本分配材料分配明细孙表
 */
public class CostAllocationDirectMaterialSumOrder_S {
    private String code;
    private BigDecimal quantity;
    private BigDecimal price;
    private BigDecimal  amount;
    private BigDecimal nassignedquantity;
    private BigDecimal nassignedcost;
    private BigDecimal  assignedquantity;
    private BigDecimal  assignedcost;
    private BigDecimal balancequantity;
    private BigDecimal balancecost;
    private BigDecimal origassignedquantity;
    private BigDecimal totalrdrecordassignedquantity;
    private BigDecimal totalinventoryassignedquantity;
    private BigDecimal sequencenumber;
    private BigDecimal ckcost;
    private String sourceVoucherCode;
    private Integer id;
    private Integer idinventory;
    private Integer idbaseunit;
    private Integer voucherId;
    private Integer idCostAllocationDirectMaterialSumDetailDTO;
    private Integer sourceVoucherId;
    private Integer sourceVoucherDetailId;
    private Date createdtime;
    private Date sourcevoucherdate;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getNassignedquantity() {
        return nassignedquantity;
    }

    public void setNassignedquantity(BigDecimal nassignedquantity) {
        this.nassignedquantity = nassignedquantity;
    }

    public BigDecimal getNassignedcost() {
        return nassignedcost;
    }

    public void setNassignedcost(BigDecimal nassignedcost) {
        this.nassignedcost = nassignedcost;
    }

    public BigDecimal getAssignedquantity() {
        return assignedquantity;
    }

    public void setAssignedquantity(BigDecimal assignedquantity) {
        this.assignedquantity = assignedquantity;
    }

    public BigDecimal getAssignedcost() {
        return assignedcost;
    }

    public void setAssignedcost(BigDecimal assignedcost) {
        this.assignedcost = assignedcost;
    }

    public BigDecimal getBalancequantity() {
        return balancequantity;
    }

    public void setBalancequantity(BigDecimal balancequantity) {
        this.balancequantity = balancequantity;
    }

    public BigDecimal getBalancecost() {
        return balancecost;
    }

    public void setBalancecost(BigDecimal balancecost) {
        this.balancecost = balancecost;
    }

    public BigDecimal getOrigassignedquantity() {
        return origassignedquantity;
    }

    public void setOrigassignedquantity(BigDecimal origassignedquantity) {
        this.origassignedquantity = origassignedquantity;
    }

    public BigDecimal getTotalrdrecordassignedquantity() {
        return totalrdrecordassignedquantity;
    }

    public void setTotalrdrecordassignedquantity(BigDecimal totalrdrecordassignedquantity) {
        this.totalrdrecordassignedquantity = totalrdrecordassignedquantity;
    }

    public BigDecimal getTotalinventoryassignedquantity() {
        return totalinventoryassignedquantity;
    }

    public void setTotalinventoryassignedquantity(BigDecimal totalinventoryassignedquantity) {
        this.totalinventoryassignedquantity = totalinventoryassignedquantity;
    }

    public BigDecimal getSequencenumber() {
        return sequencenumber;
    }

    public void setSequencenumber(BigDecimal sequencenumber) {
        this.sequencenumber = sequencenumber;
    }

    public BigDecimal getCkcost() {
        return ckcost;
    }

    public void setCkcost(BigDecimal ckcost) {
        this.ckcost = ckcost;
    }

    public String getSourceVoucherCode() {
        return sourceVoucherCode;
    }

    public void setSourceVoucherCode(String sourceVoucherCode) {
        this.sourceVoucherCode = sourceVoucherCode;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdinventory() {
        return idinventory;
    }

    public void setIdinventory(Integer idinventory) {
        this.idinventory = idinventory;
    }

    public Integer getIdbaseunit() {
        return idbaseunit;
    }

    public void setIdbaseunit(Integer idbaseunit) {
        this.idbaseunit = idbaseunit;
    }

    public Integer getVoucherId() {
        return voucherId;
    }

    public void setVoucherId(Integer voucherId) {
        this.voucherId = voucherId;
    }

    public Integer getIdCostAllocationDirectMaterialSumDetailDTO() {
        return idCostAllocationDirectMaterialSumDetailDTO;
    }

    public void setIdCostAllocationDirectMaterialSumDetailDTO(Integer idCostAllocationDirectMaterialSumDetailDTO) {
        this.idCostAllocationDirectMaterialSumDetailDTO = idCostAllocationDirectMaterialSumDetailDTO;
    }

    public Integer getSourceVoucherId() {
        return sourceVoucherId;
    }

    public void setSourceVoucherId(Integer sourceVoucherId) {
        this.sourceVoucherId = sourceVoucherId;
    }

    public Integer getSourceVoucherDetailId() {
        return sourceVoucherDetailId;
    }

    public void setSourceVoucherDetailId(Integer sourceVoucherDetailId) {
        this.sourceVoucherDetailId = sourceVoucherDetailId;
    }

    public Date getCreatedtime() {
        return createdtime;
    }

    public void setCreatedtime(Date createdtime) {
        this.createdtime = createdtime;
    }

    public Date getSourcevoucherdate() {
        return sourcevoucherdate;
    }

    public void setSourcevoucherdate(Date sourcevoucherdate) {
        this.sourcevoucherdate = sourcevoucherdate;
    }
}
