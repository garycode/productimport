package com.qihunet.tplus.productimport.entity;

import java.math.BigDecimal;

public class CostAllocationOrder_B_tmp {
    private Integer id;
    private String code;
    private String rkdcode;
    private String rkdid;
            private String jgdcode;
            private Integer jgdid;
            private BigDecimal refcost;
            private BigDecimal quantity;
            private BigDecimal totalrefcost;
            private Integer idbaseunit;
            private Integer idinventory;
            private Integer idwarehouse;
            private Integer SourceVoucherTypeId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getRkdcode() {
        return rkdcode;
    }

    public void setRkdcode(String rkdcode) {
        this.rkdcode = rkdcode;
    }

    public String getRkdid() {
        return rkdid;
    }

    public void setRkdid(String rkdid) {
        this.rkdid = rkdid;
    }

    public String getJgdcode() {
        return jgdcode;
    }

    public void setJgdcode(String jgdcode) {
        this.jgdcode = jgdcode;
    }

    public Integer getJgdid() {
        return jgdid;
    }

    public void setJgdid(Integer jgdid) {
        this.jgdid = jgdid;
    }

    public BigDecimal getRefcost() {
        return refcost;
    }

    public void setRefcost(BigDecimal refcost) {
        this.refcost = refcost;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getTotalrefcost() {
        return totalrefcost;
    }

    public void setTotalrefcost(BigDecimal totalrefcost) {
        this.totalrefcost = totalrefcost;
    }

    public Integer getIdbaseunit() {
        return idbaseunit;
    }

    public void setIdbaseunit(Integer idbaseunit) {
        this.idbaseunit = idbaseunit;
    }

    public Integer getIdinventory() {
        return idinventory;
    }

    public void setIdinventory(Integer idinventory) {
        this.idinventory = idinventory;
    }

    public Integer getIdwarehouse() {
        return idwarehouse;
    }

    public void setIdwarehouse(Integer idwarehouse) {
        this.idwarehouse = idwarehouse;
    }

    public Integer getSourceVoucherTypeId() {
        return SourceVoucherTypeId;
    }

    public void setSourceVoucherTypeId(Integer sourceVoucherTypeId) {
        SourceVoucherTypeId = sourceVoucherTypeId;
    }
}
