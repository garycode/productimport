package com.qihunet.tplus.productimport.entity;

import sun.awt.image.PixelConverter;

import java.math.BigDecimal;

/**
 * 成品入库明细信息
 */
public class RDRecordDetailDTO {
    private Integer ID;
    private String Warehouse_Name;
    private String Inventory_Code;
    private String Inventory_Name;
    private String Inventory_Specification;
    private String BaseUnit_Name;
    private BigDecimal baseQuantity;
    private String SourceVoucherType_Name;
    private Integer idRDRecordDTO;
    private String RDRecord_Code;
    private String Code;
    private Integer  SourceVoucherType_id;
    private Integer  RDRecord_id;
    private Integer  Inventory_id;
    private Integer  BaseUnit_id;
    private Integer  Warehouse_id;
    private Integer jgdId;
    private String BaseUnit_Code;
    private String Warehouse_Code;
    private BigDecimal refcost;
    private BigDecimal totalrefcost;
    private String jgdCode;
    private Integer jgdDetailId;


    public BigDecimal getRefcost() {
        return refcost;
    }

    public void setRefcost(BigDecimal refcost) {
        this.refcost = refcost;
    }

    public BigDecimal getTotalrefcost() {
        return totalrefcost;
    }

    public void setTotalrefcost(BigDecimal totalrefcost) {
        this.totalrefcost = totalrefcost;
    }

    public String getJgdCode() {
        return jgdCode;
    }

    public void setJgdCode(String jgdCode) {
        this.jgdCode = jgdCode;
    }

    public Integer getJgdDetailId() {
        return jgdDetailId;
    }

    public void setJgdDetailId(Integer jgdDetailId) {
        this.jgdDetailId = jgdDetailId;
    }

    public String getWarehouse_Code() {
        return Warehouse_Code;
    }

    public void setWarehouse_Code(String warehouse_Code) {
        Warehouse_Code = warehouse_Code;
    }

    public String getBaseUnit_Code() {
        return BaseUnit_Code;
    }

    public void setBaseUnit_Code(String baseUnit_Code) {
        BaseUnit_Code = baseUnit_Code;
    }

    public Integer getJgdId() {
        return jgdId;
    }

    public void setJgdId(Integer jgdId) {
        this.jgdId = jgdId;
    }

    public Integer getID() {
        return ID;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    public String getWarehouse_Name() {
        return Warehouse_Name;
    }

    public void setWarehouse_Name(String warehouse_Name) {
        Warehouse_Name = warehouse_Name;
    }

    public String getInventory_Code() {
        return Inventory_Code;
    }

    public void setInventory_Code(String inventory_Code) {
        Inventory_Code = inventory_Code;
    }

    public String getInventory_Name() {
        return Inventory_Name;
    }

    public void setInventory_Name(String inventory_Name) {
        Inventory_Name = inventory_Name;
    }

    public String getInventory_Specification() {
        return Inventory_Specification;
    }

    public void setInventory_Specification(String inventory_Specification) {
        Inventory_Specification = inventory_Specification;
    }

    public String getBaseUnit_Name() {
        return BaseUnit_Name;
    }

    public void setBaseUnit_Name(String baseUnit_Name) {
        BaseUnit_Name = baseUnit_Name;
    }

    public BigDecimal getBaseQuantity() {
        return baseQuantity;
    }

    public void setBaseQuantity(BigDecimal baseQuantity) {
        this.baseQuantity = baseQuantity;
    }

    public String getSourceVoucherType_Name() {
        return SourceVoucherType_Name;
    }

    public void setSourceVoucherType_Name(String sourceVoucherType_Name) {
        SourceVoucherType_Name = sourceVoucherType_Name;
    }

    public Integer getIdRDRecordDTO() {
        return idRDRecordDTO;
    }

    public void setIdRDRecordDTO(Integer idRDRecordDTO) {
        this.idRDRecordDTO = idRDRecordDTO;
    }

    public String getRDRecord_Code() {
        return RDRecord_Code;
    }

    public void setRDRecord_Code(String RDRecord_Code) {
        this.RDRecord_Code = RDRecord_Code;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    public Integer getSourceVoucherType_id() {
        return SourceVoucherType_id;
    }

    public void setSourceVoucherType_id(Integer sourceVoucherType_id) {
        SourceVoucherType_id = sourceVoucherType_id;
    }

    public Integer getRDRecord_id() {
        return RDRecord_id;
    }

    public void setRDRecord_id(Integer RDRecord_id) {
        this.RDRecord_id = RDRecord_id;
    }

    public Integer getInventory_id() {
        return Inventory_id;
    }

    public void setInventory_id(Integer inventory_id) {
        Inventory_id = inventory_id;
    }

    public Integer getBaseUnit_id() {
        return BaseUnit_id;
    }

    public void setBaseUnit_id(Integer baseUnit_id) {
        BaseUnit_id = baseUnit_id;
    }

    public Integer getWarehouse_id() {
        return Warehouse_id;
    }

    public void setWarehouse_id(Integer warehouse_id) {
        Warehouse_id = warehouse_id;
    }
}
