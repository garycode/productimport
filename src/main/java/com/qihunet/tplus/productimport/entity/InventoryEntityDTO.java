package com.qihunet.tplus.productimport.entity;

import java.io.Serializable;
import java.math.BigDecimal;

public class InventoryEntityDTO implements Serializable {
    private Integer id;
    private String code;
    private String name;
    private String shorthand;
    private String disabled;
    private Integer idunit;
    private Integer idwarehouse;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShorthand() {
        return shorthand;
    }

    public void setShorthand(String shorthand) {
        this.shorthand = shorthand;
    }


    public String getDisabled() {
        return disabled;
    }

    public void setDisabled(String disabled) {
        this.disabled = disabled;
    }

    public Integer getIdunit() {
        return idunit;
    }

    public void setIdunit(Integer idunit) {
        this.idunit = idunit;
    }

    public Integer getIdwarehouse() {
        return idwarehouse;
    }

    public void setIdwarehouse(Integer idwarehouse) {
        this.idwarehouse = idwarehouse;
    }

    @Override
    public String toString() {
        return "InventoryEntityDTO{" +
                "id=" + id +
                ", code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", shorthand='" + shorthand + '\'' +
                ", disabled='" + disabled + '\'' +
                ", idunit=" + idunit +
                ", idwarehouse=" + idwarehouse +
                '}';
    }
}