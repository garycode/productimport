package com.qihunet.tplus.productimport.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

/**
 * 产品成本分配表
 */
public class CostAllocationOrder implements Serializable{
    private String code;
    /**
     * 凭证编号
     */
    private String docno;
    /**
     * 凭证类别 
     */
    private String docclass;

    /**
     * 会计期间
     */
    private Integer accountingperiod;
    /***
     * 凭证ID
     */
    private String docid;
    /**
     * 会计年度
     */
    private Integer accountingyear;
    /**
     * 备注
     */
    private String memo;
    /**
     * 制单人
     */
    private String maker;

    /**
     * 修改人
     */
    private String reviser;

    /**
     * 是否已结转
     */
    private short iscarriedforwardout;
    /**
     * 是否结转来的单据
     */
    private short iscarriedforwardin;
    /**
     * 是否手工修改单据编码
     */
    private short ismodifiedcode;
    /**
     * 直接材料
     */
    private BigDecimal directmaterialcost;
    /**
     * 间接材料
     */
    private BigDecimal indirectmaterialcost;
    /**
     * 制造费用
     */
    private BigDecimal manufacturcost;
    /**
     * 人工费用
     */
    private BigDecimal mancost;
    /**
     * 委外费用
     */
    private BigDecimal consignationcost;
    /**
     * 其他费用
     */
    private BigDecimal othercost;
    /**
     * 其他费用1
     */
    private BigDecimal othercost1;
    /**
     * 其他费用2
     */
    private BigDecimal othercost2;


    private Integer PrintCount;
    private Integer id;
    /**
     * 所属营销机构ID
     */
    private Integer idmarketingorgan;
    /**
     *负责人ID
     */
    private Integer idperson;
    /**
     *项目ID
     */
    private Integer idproject;
    /**
     * 分摊依据
     */
    private Integer allocationbasis;
    /**
     * 归集对象
     */
    private Integer costcollectorobject;
    /**
     * 单据状态
     */
    private Integer voucherstate;
    /**
     * 制单人ID
     */
    private Integer makerid;
    /**
     * 单据日期
     */
    private Date voucherdate;
    /**
     * 制单日期
     */
    private Date madedate;
    private Date createdtime;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDocno() {
        return docno;
    }

    public void setDocno(String docno) {
        this.docno = docno;
    }

    public String getDocclass() {
        return docclass;
    }

    public void setDocclass(String docclass) {
        this.docclass = docclass;
    }

    public Integer getAccountingperiod() {
        return accountingperiod;
    }

    public void setAccountingperiod(Integer accountingperiod) {
        this.accountingperiod = accountingperiod;
    }

    public String getDocid() {
        return docid;
    }

    public void setDocid(String docid) {
        this.docid = docid;
    }

    public Integer getAccountingyear() {
        return accountingyear;
    }

    public void setAccountingyear(Integer accountingyear) {
        this.accountingyear = accountingyear;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getMaker() {
        return maker;
    }

    public void setMaker(String maker) {
        this.maker = maker;
    }

    public String getReviser() {
        return reviser;
    }

    public void setReviser(String reviser) {
        this.reviser = reviser;
    }

    public short getIscarriedforwardout() {
        return iscarriedforwardout;
    }

    public void setIscarriedforwardout(short iscarriedforwardout) {
        this.iscarriedforwardout = iscarriedforwardout;
    }

    public short getIscarriedforwardin() {
        return iscarriedforwardin;
    }

    public void setIscarriedforwardin(short iscarriedforwardin) {
        this.iscarriedforwardin = iscarriedforwardin;
    }

    public short getIsmodifiedcode() {
        return ismodifiedcode;
    }

    public void setIsmodifiedcode(short ismodifiedcode) {
        this.ismodifiedcode = ismodifiedcode;
    }

    public BigDecimal getDirectmaterialcost() {
        return directmaterialcost;
    }

    public void setDirectmaterialcost(BigDecimal directmaterialcost) {
        this.directmaterialcost = directmaterialcost;
    }

    public BigDecimal getIndirectmaterialcost() {
        return indirectmaterialcost;
    }

    public void setIndirectmaterialcost(BigDecimal indirectmaterialcost) {
        this.indirectmaterialcost = indirectmaterialcost;
    }

    public BigDecimal getManufacturcost() {
        return manufacturcost;
    }

    public void setManufacturcost(BigDecimal manufacturcost) {
        this.manufacturcost = manufacturcost;
    }

    public BigDecimal getMancost() {
        return mancost;
    }

    public void setMancost(BigDecimal mancost) {
        this.mancost = mancost;
    }

    public BigDecimal getConsignationcost() {
        return consignationcost;
    }

    public void setConsignationcost(BigDecimal consignationcost) {
        this.consignationcost = consignationcost;
    }

    public BigDecimal getOthercost() {
        return othercost;
    }

    public void setOthercost(BigDecimal othercost) {
        this.othercost = othercost;
    }

    public BigDecimal getOthercost1() {
        return othercost1;
    }

    public void setOthercost1(BigDecimal othercost1) {
        this.othercost1 = othercost1;
    }

    public BigDecimal getOthercost2() {
        return othercost2;
    }

    public void setOthercost2(BigDecimal othercost2) {
        this.othercost2 = othercost2;
    }



    public Integer getPrintCount() {
        return PrintCount;
    }

    public void setPrintCount(Integer printCount) {
        PrintCount = printCount;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdmarketingorgan() {
        return idmarketingorgan;
    }

    public void setIdmarketingorgan(Integer idmarketingorgan) {
        this.idmarketingorgan = idmarketingorgan;
    }

    public Integer getIdperson() {
        return idperson;
    }

    public void setIdperson(Integer idperson) {
        this.idperson = idperson;
    }

    public Integer getIdproject() {
        return idproject;
    }

    public void setIdproject(Integer idproject) {
        this.idproject = idproject;
    }

    public Integer getAllocationbasis() {
        return allocationbasis;
    }

    public void setAllocationbasis(Integer allocationbasis) {
        this.allocationbasis = allocationbasis;
    }

    public Integer getCostcollectorobject() {
        return costcollectorobject;
    }

    public void setCostcollectorobject(Integer costcollectorobject) {
        this.costcollectorobject = costcollectorobject;
    }

    public Integer getVoucherstate() {
        return voucherstate;
    }

    public void setVoucherstate(Integer voucherstate) {
        this.voucherstate = voucherstate;
    }

    public Integer getMakerid() {
        return makerid;
    }

    public void setMakerid(Integer makerid) {
        this.makerid = makerid;
    }

    public Date getVoucherdate() {
        return voucherdate;
    }

    public void setVoucherdate(Date voucherdate) {
        this.voucherdate = voucherdate;
    }

    public Date getMadedate() {
        return madedate;
    }

    public void setMadedate(Date madedate) {
        this.madedate = madedate;
    }

    public Date getCreatedtime() {
        return createdtime;
    }

    public void setCreatedtime(Date createdtime) {
        this.createdtime = createdtime;
    }

    @Override
    public String toString() {
        return "CostAllocationOrder{" +
                "code='" + code + '\'' +
                ", docno='" + docno + '\'' +
                ", docclass='" + docclass + '\'' +
                ", accountingperiod=" + accountingperiod +
                ", docid='" + docid + '\'' +
                ", accountingyear=" + accountingyear +
                ", memo='" + memo + '\'' +
                ", maker=" + maker +
                ", reviser='" + reviser + '\'' +
                ", iscarriedforwardout=" + iscarriedforwardout +
                ", iscarriedforwardin=" + iscarriedforwardin +
                ", ismodifiedcode=" + ismodifiedcode +
                ", directmaterialcost=" + directmaterialcost +
                ", indirectmaterialcost=" + indirectmaterialcost +
                ", manufacturcost=" + manufacturcost +
                ", mancost=" + mancost +
                ", consignationcost=" + consignationcost +
                ", othercost=" + othercost +
                ", othercost1=" + othercost1 +
                ", othercost2=" + othercost2 +
                ", PrintCount=" + PrintCount +
                ", id=" + id +
                ", idmarketingorgan=" + idmarketingorgan +
                ", idperson=" + idperson +
                ", idproject=" + idproject +
                ", allocationbasis=" + allocationbasis +
                ", costcollectorobject=" + costcollectorobject +
                ", voucherstate=" + voucherstate +
                ", makerid=" + makerid +
                ", voucherdate=" + voucherdate +
                ", madedate=" + madedate +
                ", createdtime=" + createdtime +
                '}';
    }
}
