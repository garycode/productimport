package com.qihunet.tplus.productimport.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 产品成本分配明细表
 */
public class CostAllocationOrderDetails implements Serializable {

    private String code;
    /**
     *来源单据编号
     */
    private String sourceVoucherCode;

    /**
     *来源单据明细时间戳
     */
    private String sourcevoucherdetailts;

    /**
     * 来源单据头时间戳
     */
    private String  sourcevoucherts;
    /**
     * 批号
     */
    private String batch;
    /**
     * 参考单位成本
     */
    private BigDecimal refcost;
    /**
     *参考总成本
     */
    private BigDecimal totalrefcost;
    /**
     * 数量
     */
    private BigDecimal quantity;
    /**
     * 直接材料
     */
    private BigDecimal directmaterials;
    /**
     * 间接材料
     */
    private BigDecimal  indirectmaterials;

    private BigDecimal  manufacturecost;
    private BigDecimal  mancost;
    private BigDecimal  outsourcingcost;
    private BigDecimal  othercost;
    private BigDecimal  othercost1;
    private BigDecimal  othercost2;
    private BigDecimal  totalcost;
    private BigDecimal  unitcost;
    /**
     * 加工单号
     */
    private String  jgdcode;
    /**
     * 入库单号
     */
    private String rkdcode;

    private Date ts;

    private Integer id;
    private Integer idinventory;
    private Integer idproject;
    private Integer idbaseunit;
    private Integer idWarehouse;
    private Integer idCostAllocationOrderDTO;
    /**
     * 加工单ID
     */
    private Integer jgdid;
    /**
     * 加工单明细ID
     */
    private Integer jgdDetailId;
    private Integer rkdid;
    private Integer sourceVoucherId;
    private Integer rkdDetailId;
    private Integer sourceVoucherDetailId;
    private Date createdtime;
    private Integer SourceVoucherTypeId;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getSourceVoucherCode() {
        return sourceVoucherCode;
    }

    public void setSourceVoucherCode(String sourceVoucherCode) {
        this.sourceVoucherCode = sourceVoucherCode;
    }

    public String getSourcevoucherdetailts() {
        return sourcevoucherdetailts;
    }

    public void setSourcevoucherdetailts(String sourcevoucherdetailts) {
        this.sourcevoucherdetailts = sourcevoucherdetailts;
    }

    public String getSourcevoucherts() {
        return sourcevoucherts;
    }

    public void setSourcevoucherts(String sourcevoucherts) {
        this.sourcevoucherts = sourcevoucherts;
    }

    public String getBatch() {
        return batch;
    }

    public void setBatch(String batch) {
        this.batch = batch;
    }

    public BigDecimal getRefcost() {
        return refcost;
    }

    public void setRefcost(BigDecimal refcost) {
        this.refcost = refcost;
    }

    public BigDecimal getTotalrefcost() {
        return totalrefcost;
    }

    public void setTotalrefcost(BigDecimal totalrefcost) {
        this.totalrefcost = totalrefcost;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getDirectmaterials() {
        return directmaterials;
    }

    public void setDirectmaterials(BigDecimal directmaterials) {
        this.directmaterials = directmaterials;
    }

    public BigDecimal getIndirectmaterials() {
        return indirectmaterials;
    }

    public void setIndirectmaterials(BigDecimal indirectmaterials) {
        this.indirectmaterials = indirectmaterials;
    }

    public BigDecimal getManufacturecost() {
        return manufacturecost;
    }

    public void setManufacturecost(BigDecimal manufacturecost) {
        this.manufacturecost = manufacturecost;
    }

    public BigDecimal getMancost() {
        return mancost;
    }

    public void setMancost(BigDecimal mancost) {
        this.mancost = mancost;
    }

    public BigDecimal getOutsourcingcost() {
        return outsourcingcost;
    }

    public void setOutsourcingcost(BigDecimal outsourcingcost) {
        this.outsourcingcost = outsourcingcost;
    }

    public BigDecimal getOthercost() {
        return othercost;
    }

    public void setOthercost(BigDecimal othercost) {
        this.othercost = othercost;
    }

    public BigDecimal getOthercost1() {
        return othercost1;
    }

    public void setOthercost1(BigDecimal othercost1) {
        this.othercost1 = othercost1;
    }

    public BigDecimal getOthercost2() {
        return othercost2;
    }

    public void setOthercost2(BigDecimal othercost2) {
        this.othercost2 = othercost2;
    }

    public BigDecimal getTotalcost() {
        return totalcost;
    }

    public void setTotalcost(BigDecimal totalcost) {
        this.totalcost = totalcost;
    }

    public BigDecimal getUnitcost() {
        return unitcost;
    }

    public void setUnitcost(BigDecimal unitcost) {
        this.unitcost = unitcost;
    }

    public String getJgdcode() {
        return jgdcode;
    }

    public void setJgdcode(String jgdcode) {
        this.jgdcode = jgdcode;
    }

    public String getRkdcode() {
        return rkdcode;
    }

    public void setRkdcode(String rkdcode) {
        this.rkdcode = rkdcode;
    }

    public Date getTs() {
        return ts;
    }

    public void setTs(Date ts) {
        this.ts = ts;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdinventory() {
        return idinventory;
    }

    public void setIdinventory(Integer idinventory) {
        this.idinventory = idinventory;
    }

    public Integer getIdproject() {
        return idproject;
    }

    public void setIdproject(Integer idproject) {
        this.idproject = idproject;
    }

    public Integer getIdbaseunit() {
        return idbaseunit;
    }

    public void setIdbaseunit(Integer idbaseunit) {
        this.idbaseunit = idbaseunit;
    }

    public Integer getIdWarehouse() {
        return idWarehouse;
    }

    public void setIdWarehouse(Integer idWarehouse) {
        this.idWarehouse = idWarehouse;
    }

    public Integer getIdCostAllocationOrderDTO() {
        return idCostAllocationOrderDTO;
    }

    public void setIdCostAllocationOrderDTO(Integer idCostAllocationOrderDTO) {
        this.idCostAllocationOrderDTO = idCostAllocationOrderDTO;
    }

    public Integer getJgdid() {
        return jgdid;
    }

    public void setJgdid(Integer jgdid) {
        this.jgdid = jgdid;
    }

    public Integer getJgdDetailId() {
        return jgdDetailId;
    }

    public void setJgdDetailId(Integer jgdDetailId) {
        this.jgdDetailId = jgdDetailId;
    }

    public Integer getRkdid() {
        return rkdid;
    }

    public void setRkdid(Integer rkdid) {
        this.rkdid = rkdid;
    }

    public Integer getSourceVoucherId() {
        return sourceVoucherId;
    }

    public void setSourceVoucherId(Integer sourceVoucherId) {
        this.sourceVoucherId = sourceVoucherId;
    }

    public Integer getRkdDetailId() {
        return rkdDetailId;
    }

    public void setRkdDetailId(Integer rkdDetailId) {
        this.rkdDetailId = rkdDetailId;
    }

    public Integer getSourceVoucherDetailId() {
        return sourceVoucherDetailId;
    }

    public void setSourceVoucherDetailId(Integer sourceVoucherDetailId) {
        this.sourceVoucherDetailId = sourceVoucherDetailId;
    }

    public Date getCreatedtime() {
        return createdtime;
    }

    public void setCreatedtime(Date createdtime) {
        this.createdtime = createdtime;
    }

    public Integer getSourceVoucherTypeId() {
        return SourceVoucherTypeId;
    }

    public void setSourceVoucherTypeId(Integer sourceVoucherTypeId) {
        SourceVoucherTypeId = sourceVoucherTypeId;
    }
}
