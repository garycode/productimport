package com.qihunet.tplus.productimport.controller;

import com.qihunet.tplus.productimport.entity.UserDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * @author
 * @create 2017-11-20 下午10:12
 **/
@RestController
@Api("HelloController相关api")
public class HelloController {
    static Map<Integer,UserDTO> users= Collections.synchronizedMap(new HashMap<>());

    @ApiOperation(value="获取用户列表", notes="")
    @RequestMapping(value="/user", method= RequestMethod.GET)
    public List<UserDTO> getUserList() {
        List<UserDTO> r = new ArrayList<UserDTO>(users.values());
        return r;
    }

    @ApiOperation(value="创建用户", notes="根据User对象创建用户")
    @ApiImplicitParam(name = "/user/add", value = "用户详细实体user", required = true, dataType = "UserDTO")
    @RequestMapping(value="", method=RequestMethod.POST)
    public String postUser(@RequestBody UserDTO user) {
        users.put(user.getId(), user);
        return "success";
    }

    @ApiOperation(value="获取用户详细信息", notes="根据url的id来获取用户详细信息")
    @ApiImplicitParam(name = "id", value = "用户ID", required = true, dataType = "Integer")
    @RequestMapping(value="/user/{id}", method=RequestMethod.GET)
    public UserDTO getUser(@PathVariable Integer id) {
        return users.get(id);
    }

    @ApiOperation(value="更新用户详细信息", notes="根据url的id来指定更新对象，并根据传过来的user信息来更新用户详细信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "用户ID", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "user", value = "用户详细实体user", required = true, dataType = "UserDTO")
    })
    @RequestMapping(value="/user/{id}", method=RequestMethod.PUT)
    public String putUser(@PathVariable Integer id, @RequestBody UserDTO user) {
        UserDTO u = users.get(id);
        u.setCode(System.currentTimeMillis());
        u.setName(user.getName());
        users.put(id, u);
        return "success";
    }

    @ApiOperation(value="删除用户", notes="根据url的id来指定删除对象")
    @ApiImplicitParam(name = "id", value = "用户ID", required = true, dataType = "Integer")
    @RequestMapping(value="/user/{id}", method=RequestMethod.DELETE)
    public String deleteUser(@PathVariable Integer id) {
        users.remove(id);
        return "success";
    }
}
